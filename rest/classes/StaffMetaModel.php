<?php

if(! class_exists('StaffMetaModel')){
	
	class StaffMetaModel extends GenericMetaModel{
		private $tbl;
		function __construct($tbl = 'pms_user_meta'){
			
			parent::__construct($tbl);
			$this->tbl = $tbl;
		}
		
	}
	
}//ends if class