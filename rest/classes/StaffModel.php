<?php

if(! class_exists('StaffModel')){
	
	class StaffModel extends GenericUserModel{
		private $tbl;
		private $request;
		private $saltish = 'yt*0fy&1^(6)t5%f8"w~';
		private $validTime = 10800; //3 hours
		
		function __construct($tbl = 'user'){
			
			parent::__construct($tbl, $request, $saltish, $validTime);
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
			$this->validTime = $validTime;
		} 
		
	}//class ends
}//if class exists