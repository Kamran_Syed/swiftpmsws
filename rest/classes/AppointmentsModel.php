<?php

if(! class_exists('AppointmentsModel')){
	class AppointmentsModel{
		
		private $tbl;
		private $request;
		private $saltish = '#68u@*0a%*[4};y0#6c^0~';
		private $validTime = 10800; //3 hours
		
		function __construct($tbl='', $request='', $saltish='', $validTime=''){
			
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
			$this->validTime = $validTime;
		}
		
	}//class ends
}//if class exists