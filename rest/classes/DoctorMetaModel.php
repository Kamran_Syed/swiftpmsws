<?php

if(! class_exists('DoctorMetaModel')){
	
	class DoctorMetaModel extends GenericMetaModel{
		private $tbl;
		
		function __construct($tbl = 'pms_dr_meta'){
			
			parent::__construct($tbl);
			$this->tbl = $tbl;
		}
		
	}
	
}//ends if class