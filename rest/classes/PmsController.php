<?php
/**
 * Generic Controller
 */
 
if(! class_exists('PmsController')){
	class PmsController{
		private $request;
		private $saltish = 'S$%dst637*(%)lsd6294~';
		
		function __construct($request) {
			
			$this->request = $request;
			
		}
		
		public function test(){
			$x = array('jon','jane','house','hen');
			return json_encode($x);
		}
		
		private function validateUser($email = "none", $phone = 0, $password, $token=null){
			
			if($email = "none" && $phone = 0 ){
				return array("login" => "failed","msg" => "Parameters Error");
			}
			
			$um = new UserModel();
			if($password == 'token'){
				
				return $um->validateToken($token, $email);//email should have token2
				
			}else{
				if(!$email = "none") {
					$user = $um->getUserByEmail($email);
				}
				if(!$phone = 0) {
					$user = $um->getUserByPhone($phone);
				}
				
				if(! $user) return false;
				
				if($user['password'] != sha1($password . $this->saltish)) return false;
				
			}
			
			return true;
			
		}
		
		public function authenticate() {
			
			if(! isset($_POST['email']) || ! isset($_POST['phone']) || ! isset($_POST['password']) ){
				
				return array("login" => "failed","msg" => "Parameters Error");
			}
			
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$password = $_POST['password'];
			$token = "";
			
			if(isset($_POST['token'])) $token = $_POST['token'];
			
			$um = new UserModel();
			
			if($email) {
				
				$user = $um->getUserByEmail($email);
				
				if(! $this->validateUser($email, $password, $token)){
					
					$logger->logEvent('AuthFailed', 0, 0, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], '', $p1, $p2);
					return array("login" => "failed","msg" => "Authentication Failed");
				}
			}
			
			if($phone) {
				
				$user = $um->getUserByPhone($phone);
				
				if(! $this->validateUser($phone, $password, $token)){
					
					$logger->logEvent('AuthFailed', 0, 0, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], '', $p1, $p2);
					return array("login" => "failed","msg" => "Authentication Failed");
				}
			}
			
		}

	}//class ends
}//if class exists
