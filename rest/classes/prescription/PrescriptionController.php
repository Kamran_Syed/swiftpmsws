<?php
/**
 * Prescription Controller
 */
 
if(! class_exists('PrescriptionController')){
	class PrescriptionController extends PmsController {
		private $request;
		private $tbl = "pms_prescription";
		
		public function __construct($request) {
			
			$this->request = $request;
			
		}

		public function test(){
			$x = array('Dr.jon','jane','02 Nov');
			return json_encode($x);
		}

		function getLastPrescription($drId, $pId) {
			
			$pm = new PrescriptionModel();

			return $pm->getLastPrescription($drId, $pId);
		}

		function getAllPrescriptions($drId, $pId) {

			$pm = new PrescriptionModel();

			return $pm->getAllPrescriptions($drId, $pId);
		}
		
		function createPrescription($drId, $pId, $prescDate) {

			$pm = new PrescriptionModel();

			return $pm->addPrescription($drId, $pId, $prescDate);
		}

		function updatePrescription($drId, $pId, $prescDate) {

			$pm = new PrescriptionModel();

			return $pm->updatePrescription($drId, $pId, $prescDate);
		}

	}//class ends
}//if class exists
