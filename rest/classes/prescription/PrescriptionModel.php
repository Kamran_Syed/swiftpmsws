<?php

if(! class_exists('PrescriptionModel')){
	class PrescriptionModel{
		
		private $tbl;
		private $request;
		private $saltish = '#68u@*0a%*[4};y0#6c^0~';
		
		function __construct($tbl='', $request='', $saltish=''){
			
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
		}

		//gets prescriptions within the period starting from start date till end date.
		function getPrescriptions($drId, $pId, $startDate, $endDate) {

			global $pmsdb;

			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE pId=? AND drId=? AND date>=? AND date<=?");
			$sql->execute(array($pId, $drId, $startDate, $endDate));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);

			return $r; //will be false if not found
		}

		function getLastPrescription($drId, $pId) {

			global $pmsdb;

			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE pId=? AND drId=? order by prescId desc");
			$sql->execute(array($pId, $drId));
			$r = $sql->fetch(PDO::FETCH_ASSOC);

			return $r; //will be false if not found
		}

		function getAllPrescriptions($drId, $pId) {

			global $pmsdb;

			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE pId=? AND drId=? order by prescId desc");
			$sql->execute(array($pId, $drId));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);

			return $r; //will be false if not found
		}

		function addPrescription($drId, $pId, $prescDate) {

			global $pmsdb;

			$sql = $pmsdb->prepare("INSERT into ".$this->tbl." SET drId=?, pId=?, date=? ");
			$sql->execute(array($drId, $pId, $prescDate));
		}

		function updatePrescription($drId, $pId, $prescDate) {

			global $pmsdb;

			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET drId=?, pId=?, date=? ");
			$sql->execute(array($drId, $pId, $prescDate));
		}

		function deletePrescription($prescId) {

			global $pmsdb;

			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE prescId=?");
			$sql->execute(array($prescId));
		}


		
	}//class ends
}//if class exists