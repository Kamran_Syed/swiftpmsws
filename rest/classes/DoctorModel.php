<?php

if(! class_exists('DoctorModel')){
	class DoctorModel extends GenericUserModel{
		private $tbl;
		private $request;
		private $saltish = 'y7+1t*0b$6(9)]@r4u3#-';
		private $validTime = 10800; //3 hours
		
		function __construct($tbl = 'pms_dr'){
			
			parent::__construct($tbl, $request, $saltish, $validTime);
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
			$this->validTime = $validTime;
		}
				
	}//class ends
}//if class exists
