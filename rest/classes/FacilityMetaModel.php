<?php

if(! class_exists('FacilityMetaModel')){
	
	class FacilityMetaModel extends GenericMetaModel{
		private $tbl;
		
		function __construct($tbl = 'pms_facility_meta'){
			
			parent::__construct($tbl);
			$this->tbl = $tbl;
		}
		
		function addMeta($fid, $metaKey, $metaVal){
			global $pmsdb;
			
			if(isset($fid) && isset($metaKey) && isset($metaVal)){
				//good
			}else{
				throw new Exception('Parameters Error');
			}

			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET fid=? , metaKey=? , metaVal=? ");
			$sql->execute(array($fid, $metaKey, $metaVal));
			
			return "success";
		}
		

		
		function getMeta($fid, $metaKey){
			global $pmsdb;
			
			if(empty($fid) || empty($metaKey)) throw new Exception('fid and Key must be provided');
			
			$sql = $pmsdb->prepare("SELECT metaVal FROM ".$this->tbl." WHERE metaKey=? AND fid=?");
			$sql->execute(array($metaKey, $fid));
			
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			if($r) $r = $r['metaVal'];
			/*else{
				$r = $sql->fetchAll(PDO::FETCH_ASSOC);
				if($r) $r = array_values($r);
			}*/
			
			return $r; //will be false if not found
		}
		
		function updateMeta($fid, $metaKey, $metaVal){
			global $pmsdb;
			
			if(empty($fid) || empty($metaKey)) throw new Exception('fid and Key must be provided');

			if($this->getMeta($fid, $metaKey, true)){
				$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET metaVal=? WHERE metaKey=? AND fid=?");
				$sql->execute(array($metaVal, $metaKey, $fid));
			}else{
				$this->addMeta($fid, $metaKey, $metaVal);
			}
			
			return "success";
			
		}
		
		function deleteMeta($fid, $metaKey){
			global $pmsdb;
			
			if(empty($metaKey)) throw new Exception('Key must be provided');
			
			$meta = $this->getMeta($fid, $metaKey, true);
			if(! $meta) throw new Exception('Meta Key does not exist.');
			
			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE metaKey=? AND fid=?");
			$sql->execute(array($metaKey, $fid));
			
			return "success";
			
		}
		
		
		
	}
	
}//ends if class