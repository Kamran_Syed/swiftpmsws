<?php

if(! class_exists('GenericUserModel')){
	class GenericUserModel{
		
		private $tbl;
		private $request;
		private $saltish = '6*@ow&0a%*(3);y0#6c9d~';
		private $validTime = 10800; //3 hours
		
		function __construct($tbl='', $request='', $saltish='', $validTime=''){
			
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
			$this->validTime = $validTime;
		}
		
		
		function createUser($kv){
			global $pmsdb;
			
			if(! isset($kv['email']) && !isset($kv['phone'])){
				throw new Exception('Cannot Create Account. Email or phone must be present');
			}
			
			if(empty($kv['phone'])) $kv['phone'] = $kv['email'];
			
			//$user = $this->getUserByEmail($kv['email']);
			
			$userExists = $this -> userExists($kv['email'], $kv['cellPhone'], $kv['fName'], $kv['lName'], $kv['gid']);
			
			if($userExists) throw new Exception('User already exists.');
			
			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET gid=? , fName =? , lName =? , email=? , cellPhone=?, ssn=? , password=?");
			$sql->execute(array($kv['gid'], $kv['fName'], $kv['lName'], $kv['email'],  $kv['cellPhone'],  $kv['ssn'], sha1($kv['password']. $this->saltish)));
			
			return "success";
		}
		
		function changeUserPassword($kv){
			global  $pmsdb;
			
			$user = $this->getUserByEmail($kv['email']);
			if(!$user) throw new Exception('Email '.$kv['email'].' does not exist.');
			
			if(!isset($kv['forceChange'])){
				if($user['password'] != sha1($kv['oldPassword']. $this->saltish)){
					throw new Exception('OLD password is incorrect');
				}
			}
			
			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET password=? WHERE email=?");
			$sql->execute(array(sha1($kv['password'].$this->saltish), $kv['email']));
			
			return "success";
			
		}
		
		function updateUser($kv){
			global  $pmsdb;
			
			$user = $this->getUserByEmail($kv['email']);
						if(!$user) throw new Exception('Email '.$kv['email'].' does not exist.');
			
			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET gid=? , fName =? , lName =? , email=? , cellPhone=?, ssn=? WHERE email=?");
			$sql->execute(array($kv['gid'], $kv['fName'], $kv['lName'], $kv['email'],  $kv['cellPhone'],  $kv['ssn'] ));
			
			return "success";
		}
		
		function deleteUser($email){
			global $pmsdb;
			
			$user = $this->getUserByEmail($email);
			if(!$user) throw new Exception('Email '.$email.' does not exist.');
			
			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE email=?");
			$sql->execute(array($email));
			
			return "success";
			
		}
		
		function getUserByEmail($email){
			global $pmsdb;
			
			if(empty($email)) return false;
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE email=?");
			$sql->execute(array($email));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; //will be false if not found
		}
		
		function getUserByPhone($phone){
			global $pmsdb;
			
			if(empty($phone)) return false;
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE phone=?");
			$sql->execute(array($phone));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; //will be false if not found
		}
		
		
		function getAllUsers($gid=0 , $city="any" , $state="any" , $zip="any" , $country="any" , $st=0 , $limit=1000){ //except users of passed array
			global $pmsdb;
			
			$where = " where ";
			if($gid=0 && $city="any" && $state="any" && $zip="any" && $country="any" ) throw new Exception('invalid input');
				
			if($gid !=0)
			{
				$where .= "gid={$gid}";
			}
			if($city != "any") 
			{
				$where .= "city={$city}";
			}
			if($state != "any") 
			{
				$where .= "state={$state}";
			}
			if($zip != "any")
			{
				$where .= "zip={$zip}";
			}
			if($country != "any")
			{
				$where .= "country={$country}";
			}
			
			$limit = "";
			if($limit !=0) {
				if($limit > 2000) $limit = 2000;
				$limit = " limit {$limit} {$st}";
			}
			$sql = $pmsdb->prepare("SELECT gid, fName, lName, email, cellPhone, ssn FROM ".$this->tbl." WHERE {$where} {$limit}");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r;
		}
		
		function validateAndGetUser($email, $password){

			$user = $this->getUserByEmail($email);
			if(! $user) return false;
			
			if($user['password'] != sha1($password . $this->saltish)) return false;
			
			return $user;
			
		}
		
		function issueToken($email){
			
			$user = $this->getUserByEmail($email);
			if(!$user) throw new Exception('Email '.$email.' does not exist.');
			
			$token = sha1(rand(20000, 99000) . rand(20000, 99000));
			
			$umm = new PatientMetaModel();
			$umm->updateMeta($user['id'], 'sysLoginToken', $token);
			$umm->updateMeta($user['id'], 'sysTokenValidity', $this->validTime + time());
			
			$token2 = sha1(rand(20000, 99000) . rand(20000, 99000));
			$umm->updateMeta($user['id'], 'sysLoginToken2', $token2);
			$umm->updateMeta($user['id'], 'sysToken2Validity', $this->validTime + time());
			
			return array($token, $token2);
		}
		
		function validateToken($token, $token2){
			
			$umm = new PatientMetaModel();
			$userID = $umm->getUserIDFromToken($token);
			if(!$userID) throw new Exception('Invalid Token. '.$token);
			
			$tmpToken = $umm->getMeta($userID, 'sysLoginToken', true);
			$validTill = $umm->getMeta($userID, 'sysTokenValidity', true);
			
			if($token != $tmpToken) return false;
			if(time() > $validTill) return false;
			
			$userID2 = $umm->getUserIDFromToken2($token2);
			if(!$userID2) throw new Exception('Invalid Token. '.$token2);
			
			$tmpToken2 = $umm->getMeta($userID2, 'sysLoginToken2', true);
			$validTill2 = $umm->getMeta($userID2, 'sysToken2Validity', true);
			
			if($token2 != $tmpToken2) return false;
			if(time() > $validTill2) return false;
			if($userID != $userID2) return false;
			
			return true;
			
		}
		
		function getAdminFacilityList($email){
			global $pmsdb;
			
			$user = $this->getUserByEmail($email);
			if(!$user) throw new Exception('Email '.$email.' does not exist.');
			
			$omm = new FacilityMetaModel();
			$om = new FacilityModel();
			
			$sql = $pmsdb->prepare("SELECT om.id, om.facilityName FROM ".$omm->getTable()." as omm, ".$om->getTable(). " as om WHERE omm.metaKey='facsAdmin' AND omm.metaVal=? AND om.id=omm.facsID order by om.facsName");
			$sql->execute(array($email));
			
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r;
			
		}

		function getFacility($email){
			global $pmsdb;
			
			$user = $this->getUserByEmail($email);
			if(!$user) throw new Exception('Email '.$email.' does not exist.');

			$umm = new PatientMetaModel();

			$facs = json_decode($umm->getMeta($user['id'], 'sysFacilities', true));

			return $facs;

		}
		
		function userExists($email, $phone, $fName, $lName, $gid) {
			global $pmsdb;
			
			$sql = $pmsdb->prepare("SELECT COUNT(*) as user_count FROM ".$this->tbl." WHERE email=? AND cellPhone=? AND fName =? AND lName =? AND gid =? ");
			$sql->execute(array($email, $phone, $fName, $lName, $gid));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			echo "<pre>";print_r($r);
			$u = $r['user_count'];
			if($u < 1) return false;
			echo "user exists";
			return true;
			
		}
		
		function lock($email) {
			global $pmsdb;

			$user = $this->getUserByEmail($email);
			
			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET password='lock' where userID=? ");
			$sql->execute(array($user['id']));
		}
		
		function unLock($email) {
			global $pmsdb;

			$user = $this->getUserByEmail($email);
			
			$password = 'hf$5g^Fv#0';
			
			$message = "Hi, \r\n Your new password is ". $password;

			// In case any of our lines are larger than 70 characters, we should use wordwrap()
			$message = wordwrap($message, 70, "\r\n");

			// Send
			mail($email, 'New password', $message);
			
			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET password=? where userID=? ");
			$sql->execute(array($password,$user['id']));
			
		}
	}//class ends
}//if class exists