<?php

if(! class_exists('TimeSlot')){
	
	class TimeSlot {
		private $breakTime;
		private $startTime;
		private $duration;
		private $pID;
		private $procedure;
		private $status;

		
		function __construct() {
			
			$this->breakTime = false;
			$this->startTime = time();
			$this->duration = 20 * 60;
			$this->pID = 0;
			
		}
		
		function isBreak() {
			
			return ($breakTime == true);
		}
		
		function getStartTime() {
			
			return $startTime;
		}
		
		function getDuration() {
			
			return $duration;
		}
		
		function getPatientID() {
			
			return $pID;
		}
		
		function getProcedure() {
			
			return $procedure;
		}
		
		function getStatus() {
			
			return $status;
		}
		
	}//class ends
}//if class exists