<?php

if(! class_exists('DayBlockModel')){
	
	class DayBlockModel {
		private $tbl = "pms_time_slot";
		private $appDate;
		private $drId;
		private $gId;
		private $fId;
		private $isNew = false;
		private $isLoaded = false;
		private $timeSlots = array();
		private constant $minTSDuration = 20;
		
		function __construct(){
			
			$this->appDate = "";
			$this->drId = "";
			$this->gId = "";
			$this->fId = "";
			$this->load($this->appDate);
			$this->sortTimeSlots($TS);
			
		}
		
		function load($date) {
			global $pmsdb;
			
			unset($this->timeSlots);
			
			
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE date=? ");
			
			$sql->execute(array($date));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($r as $TS) {
				$startTime = $TS['time'];
				
				$this->timeSlots[''.$startTime.''] = $TS;
			}
			
			$isLoaded = true;
			
			return $r;

		}
		
		function saveTimeSlot($TS) {
			global $pmsdb;
			$isNew = true;
			
			/* $tsExits = getTimeSlotByID($TSID);
			
			if($tsExits) return false; */
			
		
			$startTime = $TS->getStartTime();
			$this->timeSlots[''.$startTime.''] = $TS;
			
			$pId = $TS->getPatientID();
			$duration = $TS->getDuration();
			$procedure = $TS->getProcedure();
			$status = $TS->getStatus();
			
			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET gid=? , fid=? , drId=? , pId=? , date=? , time=? , duration=? , m_procedure=? , status=? ");
			$sql->execute(array($gId, $fId, $drId, $pId, $appDate, $startTime, $duration, $procedure, $status ));
			
			//$this->sortTimeSlots($TS);
			
			return true;
		}
		
		function saveTimeSlots($date , $TSArray) {
			global $pmsdb;

			foreach($TSArray as $ts) {
				$TSID = $ts['id'];
				$tsExits = getTimeSlotByID($TSID);
			
				if($tsExits) continue;
				
				$this->saveTimeSlot($ts);
				$this->sortTimeSlots($ts);
			}
			
			return true;
		}
		
		function update($TSID, $TS) {
			global $pmsdb;
			
			$tsExists = getTimeSlotByID($TSID);
			
			if(!$tsExists) throw new Exception('Time Slot does not exists.');
			
			$startTime = $TS->getStartTime();
			$this->timeSlots[''.$startTime.''] = $TS;
			
			$pId = $TS->getPatientID();
			$duration = $TS->getDuration();
			$procedure = $TS->getProcedure();
			$status = $TS->getStatus();
			
			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET gid=? , fid=? , drId=? , pId=? , date=? , time=? , duration=? , m_procedure=? , status=? WHERE id=?");
			$sql->execute(array($gId, $fId, $drId, $pId, $appDate, $startTime, $duration, $procedure, $status, $TSID ));
			
			$this->sortTimeSlots($TS);
			
			return true;
		}
		
		
		function deleteTimeSlot($TSID) {
			global $pmsdb;
			
			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE id=?");
			$sql->execute(array($TSID));
			
			$ts = getTimeSlotByID($TSID);
			
			$this->sortTimeSlots($ts);
			
			return true;

		}
		
		private function sortTimeSlots(&$TS) {
			ksort($TS);
		}
		
		private function getAllTimeSlots() {
			global $pmsdb;
			
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl."");
			
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r;
		}
		
		function getTimeSlotByDt($date, $startTime) {
			global $pmsdb;
			
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE date=? AND time=? ");
			
			$sql->execute(array($date, $startTime));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			
			return $r;
		}
		
		function getTimeSlotByID($TSID) {
			global $pmsdb;
			
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE id=? ");
			
			$sql->execute(array($TSID));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			
			return $r;
		}
		
		function slideUp($time, $duration) {
			
		}
		
		function slideDown($time, $duration) {
			
		}
		
		private function isLoaded() {
			return ($isLoaded == true);
		}
		
		private function isNew() {
			return ($isNew == true);
		}
		
		function isFull() {
			$ts = $this->timeSlots();
			
			foreach($ts as $t) {
				
			}
			
			return true;
		}
		
		function isOverlapping($date, $startTime) {
			$result = getTimeSlotByDt($date, $startTime);
			
			if($result) return true;
			
			return false;
		}
		
	}//class ends
}//if class exists