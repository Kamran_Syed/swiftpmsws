<?php

if(! class_exists('FacilityModel')){
	
	class FacilityModel{
		private $tbl = "pms_facility";
		
		function __construct(){
			
		}
		
		function createfacility($kv){
			global $pmsdb;
			
			if(! isset($kv['facilityShortName'])) $kv['facilityShortName'] = $kv['facilityName'];
			
			if(! isset($kv['facilityName']) || ! isset($kv['facilityShortName'])){
				throw new Exception('Cannot Create Account. Name and Short Name must be present');
			}
			
			$facility = $this->getFacilityByName($kv['facilityName']);
			if($facility) throw new Exception($kv['facilityName'].' already exists. Try a different one.');
			
			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET facilityName=? , facilityShortName=? , gid=? ");
			$sql->execute(array($kv['facilityName'], $kv['facilityShortName'], $kv['gid'] ));
			
			return "success";
		}
		
		function updatefacility($kv){
			global $pmsdb;
			
			$facility = $this->getFacilityByID($kv['id']);
			if(!$facility) throw new Exception('id '.$id.' does not exist.');
			
			$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET facilityName=? , facilityShortName=? , gid=? WHERE id=?");
			$sql->execute(array($kv['facilityName'], $kv['facilityShortName'], $kv['gid'], $kv['id']));
			
			return "success";
		}
		
		function deletefacility($id){
			global $pmsdb;
			
			$facility = $this->getFacilityByID($id);
			if(!$facility) throw new Exception('id '.$id.' does not exist.');
			
			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE id=?");
			$sql->execute(array($id));
			//TODO cascade delete meta
			return "success";
			
		}
		
		function getFacilityByName($facilityName){
			global $pmsdb;
			
			if(empty($facilityName)) return false;
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE facilityName=?");
			$sql->execute(array($facilityName));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			return $r; //will be false if not found
		}
		
		function getFacilityByID($id){
			global $pmsdb;
			
			if(empty($id)) return false;
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE id=?");
			$sql->execute(array($id));
			$r = $sql->fetch(PDO::FETCH_ASSOC);
			return $r;
		}
		
		function getAllOrgs(){
			global $pmsdb;
			
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." order by facilityName");
			$sql->execute();
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r;
		}
		
		function getTable(){
			
			return $this->tbl;
			
		}
		
		function addUserToFacility($id, $email){
			$um = new UserModel();
			$user = $um->getUserByEmail($email);
			if(!$user) throw new Exception('Email '.$email.' does not exist.');
			
			$fmm = new FacilityMetaModel();
			$users = $fmm->getMeta($id, 'orgUsers', true);
			if(! $users){
				$users = array();
			}else{
				$users = json_decode($users);
			} 
			
			foreach($users as $u){
				if($u->email == $email) return true;
			}
			
			$dmy['id'] = $user['id'];
			$dmy['email'] = $user['email'];
			$users[] = $dmy;
			$fmm->updateMeta($id, 'facilityUsers', json_encode($users));
			
			return true;
			
		}
		
		function removeUserFromFacility($id, $email){
			
			$fmm = new FacilityMetaModel();
			$users = $fmm->getMeta($id, 'facilityUsers', true);
			if(! $users){
				return false;
			}else{
				$users = json_decode($users);
			} 
			
			$newUsers = array();
			foreach($users as $u){
				if($u->email == $email) continue;
				$newUsers[] = $u;
			}
			
			$fmm->updateMeta($id, 'facilityUsers', json_encode($newUsers));
			
			return true;
			
		}
		
		function getAllUsers($id){
			
			$fmm = new FacilityMetaModel();
			$users = $fmm->getMeta($id, 'FacilityUsers', true);
			if(! $users){
				return false;
			}else{
				$users = json_decode($users);
			}

			return $users;
			
		}
	
		
	}
	
}//ends if class