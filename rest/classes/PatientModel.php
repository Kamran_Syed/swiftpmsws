<?php

if(! class_exists('PatientModel')){
	
	class PatientModel extends GenericUserModel{
		private $tbl;
		private $request;
		private $saltish = '6*@ow&0a%*(3);y0#6c9d~';
		private $validTime = 10800; //3 hours
		
		function __construct($tbl = 'pms_patient'){
			
			parent::__construct($tbl, $request, $saltish, $validTime);
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
			$this->validTime = $validTime;
		}
		
	}//class ends
}//if class exists
