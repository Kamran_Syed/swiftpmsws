<?php

if(! class_exists('EventLoggerModel')){
	
	class EventLoggerModel{
		private $tbl = "pms_eventLogger";
		private $eventTypes = array();
		
		public static function validateDate($dt){
			$date = date_parse($dt);
			if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"])){
				return true;
			}else{
				return false;
			}
			return false;	
		}
		
		
		function __construct(){
			
			$this->eventTypes = array(
				"Login",
				"Logout",
				"AccessPointMessage",
				"ResourceAuthSuccess",
				"ResourceAuthFailed",
				"AuthFailed",
				"AdminLogin",
				"AdminAuthFailed"
			);
		}
		
		/**
		 *  generic function for logging
		 *  p1 and p2 are event specific
		 */
		public function logEvent($eventType, $userID, $fID, $ipAddress, $ua, $moreinfo='', $p1='', $p2=''){
			global $pmsdb;
			
			if($eventType && $ipAddress && $ua){
				if(! in_array($eventType, $this->eventTypes)){
					throw new Exception('Invalid event type '.$eventType);
				}
			}else{
				throw new Exception('Logging data is missing mandatory information.');
			}
			
			$dt = date('Y-m-d H:i:s');
			//if(empty($moreinfo)) $moreinfo = json_encode(get_browser());
			
			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET dt=?, eventType=? , userID=?, fID=?, ipAddress=? , ua=? , moreinfo=? , p1=? , p2=? ");
			$sql->execute(array($dt, $eventType, $userID, $fID, $ipAddress, $ua, $moreinfo, $p1, $p2));
			
			return true;
		}
		
		public function getLog($start, $end, $eventType, $userID, $fID=0, $p1='', $p2='', $limit=100){
			global $pmsdb;
			
			if(! EventLoggerModel::validateDate($start)){
				throw new Exception('Invalid start date '.$start);
				
			}elseif(! EventLoggerModel::validateDate($end)){
				throw new Exception('Invalid end date '.$end);
				
			}elseif(! in_array($eventType, $this->eventTypes) && $eventType != 'any'){
				throw new Exception('Invalid event type '.$eventType);
			}
			
			$sqlOrgID = ($fID==0)?'':' and fID='.$fID;
			$sqlP1 = ($p1=='')?'':" and p1='".$p1."' ";
			$sqlP2 = ($p2=='')?'':" and p2='".$p2."' ";
			$sqlEventType = ($eventType=='any')?'':" and eventType = '".$eventType."' ";
			
			$sql = "Select * from ".$this->tbl." where dt >= '".$start."' and dt <='".$end."' and userID=".$userID
					.$sqlfID.$sqlP1.$sqlP2.$sqlEventType. " limit ".$limit;
			
			$stmt = $pmsdb->query($sql);
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			return $results;
			
		}
		
		
		
	
		
	} //class ends
	
}//ends if class