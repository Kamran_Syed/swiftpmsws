<?php
/**
 * Patient Controller
 */
 
if(! class_exists('PatientController')){
	class PatientController extends PmsController {
		private $request;
		private $tbl = "pms_patient";
		
		public function __construct($request) {
			
			$this->request = $request;
			
		}

		/*public function test(){
			$gid= 3;
			$password ="iamareeba";
			$user_fname = $_POST['firstname'];
			$user_lname = $_POST['lastname'];
			$user_email = $_POST['email'];
			$user_phone = $_POST['phone'];
			$user_ssn = $_POST['ssn'];
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
			global $pmsdb;
			//mail('areebakamrn@gmail.com','debug from srv', print_r($_POST, true));
			//throw new RestException(305, $_POST['firstname']);
			
			//$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET  gid=? , fName =? , lName =? , email=? , cellPhone=?, ssn=? , password=?");
			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET  gid=? , fName ='adsg' , lName ='gihj' , email='areebakamrn@gmail' , cellPhone=8957, ssn=78665 , password=?");
			//$sql->execute(array($gid, $fid,$user_fname, $user_lname, $user_email, $user_phone , $user_ssn, $password));
			$sql->execute(array($gid,$password));
				
			
			  $x = array('i','am','working','hard');
			return json_encode($x); 
		}*/
		
		public function authenticate($additionalInfo=false)
    {
		
		if(! isset($_POST['loginID']) || ! isset($_POST['password'])){
			return array("login" => "failed","msg" => "Parameters Error");
		}
		
        $loginID = $_POST['loginID'];
        $password = $_POST['password'];
        $token = "";
		$sessionId = $_POST['sessionID'];
		
		if(isset($_POST['token'])) $token = $_POST['token'];
		
		if(!$sessionId) throw new RestException(408, "Session ID not found.");
        
		$um = new UserModel();
		$user = $um->getUserByLoginID($loginID);
		$logger = new EventLoggerModel();
		
		
		if(! $this->validateUser($loginID, $password, $token)){
			
			$p1 = "Invalid User ".$loginID;
			if($user) {
				$p1="Valid User ".$loginID;
				
			}
			$logger->logEvent('AuthFailed', 0, 0, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], '', $p1);
			
			return array("login" => "failed","msg" => "Authentication Failed");
		}
		
		$userID = $user['id'];
		$umm = new UserMetaModel();
		$facIDs = json_decode($umm->getMeta($userID, 'sysFacilities')); //has to be checked
	
		//TODO  getOrgMeta($orgID,  , false);
		$facMeta = array("supportPhone" => "7657373","address" => "Green area, noway street Hawai");
		$facMeta['facIDs'] = $facIDs;

		$token = $um->issueToken($loginID);
		$finalList['token'] = $token[0];
		$finalList['token2'] = $token[1];
		
		$finalList['facMeta'] = $orgMeta;
		if($additionalInfo) $finalList['userID'] = $userID;
		$finalList['login'] = "success";
		


		if(! $additionalInfo){
			$logger->logEvent('Login', $userID, 0, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], '', '' );
			
		}
		
		return $finalList;
		
    }
	
	  public function logout()
    {
		$userId = $_POST['loginID'];
		
		$um = new UserModel();
		$user = $um->getUserByLoginID($userId);
		
		$logger = new EventLoggerModel();
		$logger->logEvent('Logout', $user['id'], 0, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $moreinfo='', $userId.' logged out.');
		
		return array("status"=>"OK");
	}
		
	 public function updatesettings()
    {
		$ret = array();
		
		$um = new UserModel();
		$umm = new UserMetaModel();
		
		$user = $um->getUserByLoginID($_POST['loginID']);
		
		$user['email'] = $_POST['email'];
		
		try{
			$rslt = $um->updateUser($user);
			$umm->updateMeta($user['id'], 'settings', json_encode(array('cellPhone' => $_POST['cellPhone'])));
			
		}catch(Exception $e){
			$ret['changed'] = "failed";
			$ret['msg'] = $e->getMessage();
			return $ret;
		}
		
		
		if($rslt == "success"){
			unset($user['password']);
			unset($user['facID']);
			unset($user['active']);
			
			$tmpSettings = $umm->getMeta($user['id'], 'settings');
			
			if($tmpSettings){
				$tmpSettings = (array) json_decode($tmpSettings);
				
				$settings = array_merge($user, $tmpSettings);
			}else{
				$settings = $user;
			}
			
			$ret['changed'] = "success";
			$ret['msg'] = "Settings saved successfully.";
			$ret['settings'] = $settings;
		}else{
			$ret['changed'] = "failed";
			$ret['msg'] = "Cannot save settings.";
		}
		
		return $ret;
	}	
	
	 public function showAppointments(){
		 
		 $pam = new PatientAppointmentModel();
		 $appointments = $pam->getAppointments($_POST['pId']);
		 return $appointments;
		 
	 }
	
	private function validateUser($loginID, $password, $token=null){

		$um = new UserModel();
		if($password == 'token'){
			
			return $um->validateToken($token, $loginID);//loginID should have token2
			
		}else{
			
			$user = $um->getUserByLoginID($loginID);
			if(! $user) return false;
			
			if($user['password'] != sha1($password . $this->saltish)) return false;
			
		}
		
		return true;
		
	}
	
	
	
	
		
	}//class ends
}//if class exists
