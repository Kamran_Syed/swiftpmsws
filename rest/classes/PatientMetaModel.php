<?php

if(! class_exists('PatientMetaModel')){
	
	class PatientMetaModel extends GenericMetaModel{
		private $tbl;
		
		function __construct($tbl = 'pms_patient_meta'){
			
			parent::__construct($tbl);
			$this->tbl = $tbl;
		}
		
	}
	
}//ends if class