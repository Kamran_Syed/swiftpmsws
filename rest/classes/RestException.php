<?php
/**
 * Define a custom REST exception class
 */
class RestException
{
    
    public function __construct($httpCode=500, $msg='Internal Server Error') {
        
		$hdr = sprintf('HTTP/1.1 %d %s', $httpCode, $msg);
		header($hdr);
		
		exit;
		
    }

}

