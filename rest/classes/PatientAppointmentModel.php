<?php

if(! class_exists('PatientAppointmentModel')){
	
	class PatientAppointmentModel extends AppointmentsModel{
		private $tbl;
		private $request;
		private $saltish = '6*@ow&0a%*(3);y0#6c9d~';
		private $validTime = 10800; //3 hours
		
		function __construct($tbl = 'pms_time_slot'){
			
			parent::__construct($tbl, $request, $saltish, $validTime);
			$this->tbl = $tbl;
			$this->request = $request;
			$this->saltish = $saltish;
			$this->validTime = $validTime;
		}
		
		public function getAppointments($pId){
			
			global $pmsdb;
			$date = date('d-m-Y');
			$dateAfter3Months = date('d-m-Y', strtotime("+3 months"));
			
			if(empty($pId)) return false;
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE pId=? AND date>=? AND date<=? ");
			$sql->execute(array($pId, $date, $dateAfter3Months ));
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $r; //will be false if not found
		 
	 }
		
	}//class ends
}//if class exists
