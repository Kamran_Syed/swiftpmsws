<?php
/**
 * Doctor Controller
 */
 
if(! class_exists('DoctorController')){
	class DoctorController extends PmsController{
		private $request;
		
		public function __construct($request) {
			
			$this->request = $request;
		}
		
		function getAppointments() {
			
			/*STUB starts here*/
			$patient = array(
				'name' => 'Johnny',
				'status' => 'available', 
				'appDate' => '6 March', 
				'appTime' => '4:00pm - 5:00pm',
				'description' => 'This is dummy description',
			);
			/*STUB ends here*/
			
			return json_encode($patient);
		}

	}//class ends
}//if class exists
