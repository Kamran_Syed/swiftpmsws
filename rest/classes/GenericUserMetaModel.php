<?php

if(! class_exists('GenericMetaModel')){
	
	class GenericMetaModel{
		private $tbl;
		
		function __construct($tbl=''){
			
			$this->tbl = $tbl;
		}
		
		function getKeys(){
			global $pmsdb;
			
			$sql = $pmsdb->prepare("SELECT distinct metaKey FROM ".$this->tbl." WHERE metaKey NOT LIKE 'sys%' order by metaKey ");
			$sql->execute();
			
			$results = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			if(! $results) return false;
			
			$ret = array();
			foreach($results as $r){
				
				$ret[] = $r['metaKey'];
			}
			
			return $ret;
			
		}
		
		
		function getTable(){
			
			return $this->tbl;
			
		}
		
		/**
		 *  $id is userid or fID
		 *  meta model must override
		 *  this method.
		 */
		 function getAllMeta($userID=0, $fID=0){
			global $pmsdb;
			
			if($userID && $fID) throw new Exception('ID must be provided');
			
			if($userID){
				$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE userID=? AND metaKey NOT LIKE 'sys%' ");
				$sql->execute(array($userID));
			}else{
				$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE fID=? AND metaKey NOT LIKE 'sys%' ");
				$sql->execute(array($fID));
			}
			
			
			$r = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			return $r; //will be false if not found
		} 
		
		/**
		 *  Generic Delete
		 *  $id is row id of table
		 */
		function deleteByID($id){
			global $pmsdb;
			
			if(empty($id)) throw new Exception('ID must be provided');
			
			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE id=? ");
			$sql->execute(array($id));
			
			return "success";
			
		}
		
		function addMeta($userID, $metaKey, $metaVal){
			global $pmsdb;
			
			if(isset($userID) && isset($metaKey) && isset($metaVal)){
				//good
			}else{
				throw new Exception('Parameters Error');
			}

			$sql = $pmsdb->prepare("INSERT INTO ".$this->tbl." SET userID=? , metaKey=? , metaVal=? ");
			$sql->execute(array($userID, $metaKey, $metaVal));
			
			return "success";
		}
		

		
		function getMeta($userID, $metaKey){
			global $pmsdb;
			
			if(empty($userID) || empty($metaKey)) throw new Exception('userID and Key must be provided');
			
			$sql = $pmsdb->prepare("SELECT metaVal FROM ".$this->tbl." WHERE metaKey=? AND userID=?");
			$sql->execute(array($metaKey, $userID));
			
			
				$r = $sql->fetch(PDO::FETCH_ASSOC);
				
				if($r) $r = $r['metaVal'];
			
				/* $r = $sql->fetchAll(PDO::FETCH_ASSOC);
				if($r) $r = array_values($r); */
			
			
			return $r; //will be false if not found
		}
		
		function getAllMetas($userID){
			global $pmsdb;
			if(empty($userID)) throw new Exception('userID  must be provided');
			
			$sql = $pmsdb->prepare("SELECT * FROM ".$this->tbl." WHERE userID=?");
			$sql->execute(array($userID));
			
			
				$r = $sql->fetch(PDO::FETCH_ASSOC);
				
				if($r) $r = $r['metaVal'];
			/* else{
				$r = $sql->fetchAll(PDO::FETCH_ASSOC);
				if($r) $r = array_values($r);
			} */
			
			return $r; //will be false if not found
		}
		
		function updateMeta($userID, $metaKey, $metaVal){
			global $pmsdb;
			
			if(empty($userID) || empty($metaKey)) throw new Exception('userID and Key must be provided');

			if($this->getMeta($userID, $metaKey, true)){
				$sql = $pmsdb->prepare("UPDATE ".$this->tbl." SET metaVal=? WHERE metaKey=? AND userID=?");
				$sql->execute(array($metaVal, $metaKey, $userID));
			}else{
				$this->addMeta($userID, $metaKey, $metaVal);
			}
			
			return "success";
			
		}
		
		function deleteMeta($userID, $metaKey){
			global $pmsdb;
			
			if(empty($metaKey)) throw new Exception('Key must be provided');
			
			$meta = $this->getMeta($userID, $metaKey, true);
			if(! $meta) throw new Exception('Meta Key does not exist.');
			
			$sql = $pmsdb->prepare("DELETE FROM ".$this->tbl." WHERE metaKey=? AND userID=?");
			$sql->execute(array($metaKey, $userID));
			
			return "success";
			
		}
		
		function getUserIDFromToken($token){
			global $pmsdb;
			
			if(empty($token)) throw new Exception('Token must be provided');
			
			$sql = $pmsdb->prepare("SELECT userID FROM ".$this->tbl." WHERE metaKey='sysLoginToken' AND metaVal=? ");
			$sql->execute(array($token));

			$r = $sql->fetch(PDO::FETCH_ASSOC);
			if($r) $r = $r['userID'];
			
			
			return $r; //will be false if not found
		}
		
		function getUserIDFromToken2($token){
			global $pmsdb;
			
			if(empty($token)) throw new Exception('Token must be provided');
			
			$sql = $pmsdb->prepare("SELECT userID FROM ".$this->tbl." WHERE metaKey='sysLoginToken2' AND metaVal=? ");
			$sql->execute(array($token));

			$r = $sql->fetch(PDO::FETCH_ASSOC);
			if($r) $r = $r['userID'];
			
			
			return $r; //will be false if not found
		}
	}
	
}//ends if class