<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
/**
 *  http://api.swiftpms.com/rest/patient/
 *  /rest/patient/
 */
require_once __DIR__ .'/classes/RestException.php';

$request = $_SERVER['REQUEST_URI'];

$method = $_SERVER['REQUEST_METHOD'];

/* if($method != strtolower('post'){
	throw new RestException(304, 'go away');
} */

/**
 *  Load classes
 */
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/classes/EventLoggerModel.php';
require_once __DIR__ . '/classes/GenericUserMetaModel.php';
require_once __DIR__ . '/classes/GenericUserModel.php';
require_once __DIR__ . '/classes/DoctorModel.php';
require_once __DIR__ . '/classes/PatientModel.php';
require_once __DIR__ . '/classes/DoctorMetaModel.php';
require_once __DIR__ . '/classes/PatientMetaModel.php';
require_once __DIR__ . '/classes/FacilityMetaModel.php';
require_once __DIR__ . '/classes/prescription/PrescriptionModel.php'; 
require_once __DIR__ . '/classes/PmsController.php';
require_once __DIR__ . '/classes/PatientController.php';
require_once __DIR__ . '/classes/DoctorController.php'; 
require_once __DIR__ . '/classes/prescription/PrescriptionController.php'; 
require_once __DIR__ . '/classes/Authenticator.php';
require_once __DIR__ . '/classes/FacilityModel.php';
//require_once __DIR__ . '/classes/UserModel.php';


//Authenticate request

header('Content-Type: application/json');

switch($request){
	case '/rest/patient/':
		//load 
		try {
			
			$patientcntrl = new PatientController($request);
		
			echo $patientcntrl->test();
			
		}catch(Exception $e) {
			
			throw new RestException(501, $e->getMessage());
			
		}
		break;
	case '/rest/doctor/':
		try {
			
			$doctor = new DoctorController($request);
		
			echo $doctor->getAppointments();
		
		}catch(Exception $e) {
			
			throw new RestException(501, $e->getMessage());
			
		}
		break;
	case '/rest/prescription/':
		try {
			
			$prescription = new PrescriptionController($request);
		
			echo $prescription->test();
		
		}catch(Exception $e) {
			
			throw new RestException(501, $e->getMessage());
			
		}
		break;
	case '/rest/pms/':
		try {
	
			$ctrl = new PmsController($request);
		
			echo $ctrl->test();
		
		}catch(Exception $e) {
			
			throw new RestException(501, $e->getMessage());
			
		}
		break;
	default:
		throw new RestException(304, 'go away');
}
exit;
 
 
?>