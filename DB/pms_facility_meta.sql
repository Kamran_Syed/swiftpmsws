CREATE TABLE IF NOT EXISTS `pms_facility_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fID` int(11) NOT NULL,
  `metaKey` varchar(255) NOT NULL,
  `metaVal` text,
  PRIMARY KEY (`id`),
  KEY `orgKey` (`fID`,`metaKey`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;