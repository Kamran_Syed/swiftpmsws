CREATE TABLE pms_appointments (
  id             BigInt(20) NOT NULL AUTO_INCREMENT,
  fid            BigInt(20) NOT NULL,
  drId           BigInt(20) NOT NULL,
  startTime      DateTime NOT NULL,
  bookingMinutes Integer(11) NOT NULL,
  patientId      Integer(11) NOT NULL DEFAULT 0,
  ptStatus       Integer(2) NOT NULL DEFAULT 0,
  bookedOn       DateTime,
  bookedBy       Integer(11), 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

CREATE INDEX I_pms_appointments_fiddr 
 ON pms_appointments(fid, drId);
CREATE INDEX I_pms_appointments_patient 
 ON pms_appointments(patientId);