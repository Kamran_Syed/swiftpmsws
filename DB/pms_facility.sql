CREATE TABLE pms_facility (
  id           BigInt(20) NOT NULL AUTO_INCREMENT,
  facilityName VarChar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

