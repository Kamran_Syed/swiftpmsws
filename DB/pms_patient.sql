
CREATE TABLE pms_patient (
  id        BigInt(20) NOT NULL AUTO_INCREMENT,
  gid       Integer(11) NOT NULL,
  fid       Integer(11) NOT NULL,
  fName     VarChar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  lName     VarChar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  email     VarChar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  cellPhone VarChar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  ssn       VarChar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  password  VarChar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  address1  VarChar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  address2  VarChar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  city      VarChar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  usState   VarChar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  zip       VarChar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  country   VarChar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci, 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

