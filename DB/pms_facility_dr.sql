
CREATE TABLE pms_facility_dr (
  id   BigInt(20) NOT NULL AUTO_INCREMENT,
  drId BigInt(20) NOT NULL,
  fid  BigInt(20) NOT NULL, 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

