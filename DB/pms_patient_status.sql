
CREATE TABLE pms_patient_status (
  id       Integer(11) NOT NULL AUTO_INCREMENT,
  ptStatus VarChar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci, 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

