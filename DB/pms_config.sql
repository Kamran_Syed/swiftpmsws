CREATE TABLE pms_config (
  id   BigInt(20) NOT NULL AUTO_INCREMENT,
  gid  BigInt(20) NOT NULL,
  fid  BigInt(20) NOT NULL,
  drId BigInt(20) NOT NULL,
  cKey VarChar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  cVal VarChar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci, 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

/*
fid = 0 means any fid
drId = 0 means any dr

*/