
CREATE TABLE pms_group (
  id        BigInt(20) NOT NULL AUTO_INCREMENT,
  groupName VarChar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
  PRIMARY KEY (
      id
  )
) ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPACT DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

